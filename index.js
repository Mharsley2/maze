const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
//main key

for (let rowindex = 0; rowindex < map.length; rowindex++) {
    let row = document.createElement("div");
    row.classList.add("rowman")
    for (let columnindex = 0; columnindex < map[0].length; columnindex++) {
        let Lit = map[rowindex].charAt(columnindex);
        let column = document.createElement("div");
        if (Lit === "W") {
            column.className = "Wall"

        }
        if (Lit === " ") {
            column.className = "path"
        }
        if (Lit === "S") {
            column.className = "Start"
        }
        if (Lit === "F") {
            column.className = "Finish"
        }
        column.id = `${rowindex}-${columnindex}`

        row.appendChild(column);




    }
    var destination = document.getElementById("contamination");
    destination.appendChild(row);

}

var character = document.createElement("div");
character.id = "character";
var start = document.getElementById("9-0");
start.appendChild(character);

document.addEventListener('keydown', (event) => {
    if (!event.key.startsWith('Arrow')) return;

    const parentCell = character.parentElement;
    const currentCellCoordinates = parentCell.id.split('-')
    let nextRowIndex;
    let nextColumnIndex;
    if (event.key === "ArrowDown") {
        nextRowIndex = Number(currentCellCoordinates[0]) + 1;
        nextColumnIndex = Number(currentCellCoordinates[1]) + 0;
    } else if (event.key === "ArrowUp") {
        nextRowIndex = Number(currentCellCoordinates[0]) - 1;
        nextColumnIndex = Number(currentCellCoordinates[1]) + 0;
    } else if (event.key === "ArrowLeft") {
        nextRowIndex = Number(currentCellCoordinates[0]) + 0;
        nextColumnIndex = Number(currentCellCoordinates[1]) - 1;
    } else if (event.key === "ArrowRight") {
        nextRowIndex = Number(currentCellCoordinates[0]) + 0;
        nextColumnIndex = Number(currentCellCoordinates[1]) + 1;
    }

    const nextCell = document.getElementById(`${nextRowIndex}-${nextColumnIndex}`)
    if (nextCell === null || nextCell.classList.contains('Wall')) {
        return false;
    }

    nextCell.appendChild(character)
    if (nextCell.classList.contains("Finish")) {
        document.getElementById("winner").style.display = "block"; 
    }
})

